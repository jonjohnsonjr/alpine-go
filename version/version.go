package version

import (
	"strings"
	"unicode"
)

type Token int

const (
	Invalid       Token = -1
	DigitOrZero   Token = 0
	Digit         Token = 1
	Letter        Token = 2
	Suffix        Token = 3
	SuffixDigit   Token = 4
	RevisionDigit Token = 5
	End           Token = 6
)

type Version string

// NextToken advances the parsing state machine.  On the first call,
// it should be called with t set to Digit.
func (v *Version) NextToken(t Token) (Token, *Version) {
	var nextToken Token
	token := []rune(*v)

	switch {
	case *v == "":
		nextToken = End
	case (t == DigitOrZero || t == Digit) && unicode.IsLower(token[0]):
		nextToken = Letter
	case (t == Letter) && unicode.IsDigit(token[0]):
		nextToken = Digit
	case (t == Suffix) && unicode.IsDigit(token[0]):
		nextToken = SuffixDigit
	case token[0] == '.':
		nextToken = DigitOrZero
	case token[0] == '_':
		nextToken = Suffix
	case token[0] == '-' && token[1] == 'r':
		nextToken = RevisionDigit
	default:
		nextToken = t
	}

	var ver Version

	switch nextToken {
	case RevisionDigit:
		ver = Version(token[2:])
	case Letter:
		fallthrough
	case SuffixDigit:
		fallthrough
	case Digit:
		fallthrough
	case End:
		ver = *v
	default:
		ver = Version(token[1:])
	}

	return nextToken, &ver
}

// TODO(ariadne): Figure out why Go won't let this be const.
var (
	preSuffixes  = []string{"alpha", "beta", "pre", "rc"}
	postSuffixes = []string{"cvs", "svn", "git", "hg", "p"}
)

// GetToken performs the same function as NextToken, but also parses
// the version into an integral component.
func (v *Version) GetToken(t Token) (Token, *Version, int64) {
	nextToken := Invalid
	token := []rune(*v)
	weightedVersion := int64(0)

	if *v == "" {
		return nextToken, v, weightedVersion
	}

	i := int(0)

	switch t {
	case DigitOrZero:
		for i < len(token) && token[i] == '0' {
			i++
		}
		nextToken = Digit
		weightedVersion = int64(-i)
	case Digit:
		fallthrough
	case SuffixDigit:
		fallthrough
	case RevisionDigit:
		for i < len(token) && unicode.IsDigit(token[i]) {
			weightedVersion *= 10
			weightedVersion += int64(token[i] - '0')
			i++
		}

		if i >= 18 {
			nextToken = Invalid
			return nextToken, nil, -1
		}
	case Letter:
		weightedVersion = int64(token[i])
		i++
	case Suffix:
		nextToken = SuffixDigit

		for pos, suf := range preSuffixes {
			if strings.HasPrefix(string(*v), suf) {
				i += len(suf)
				weightedVersion -= int64(pos)
				break
			}
		}

		if i == 0 {
			for pos, suf := range postSuffixes {
				if strings.HasPrefix(string(*v), suf) {
					i += len(suf)
					weightedVersion = int64(pos)
					break
				}
			}
		}
	default:
		nextToken = Invalid
	}

	// Skip ahead by i bytes.
	token = token[i:]
	ov := Version(token)
	outVer := &ov

	if *outVer == "" {
		nextToken = End
	} else if nextToken == Invalid {
		nextToken, outVer = outVer.NextToken(t)
	}

	return nextToken, outVer, weightedVersion
}
