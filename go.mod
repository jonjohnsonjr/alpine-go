module gitlab.alpinelinux.org/alpine/go

go 1.19

require (
	github.com/MakeNowJust/heredoc v1.0.0
	github.com/stretchr/testify v1.8.0
	golang.org/x/exp v0.0.0-20221212164502-fae10dda9338
	gopkg.in/ini.v1 v1.67.0
	gopkg.in/yaml.v2 v2.4.0
	mvdan.cc/sh/v3 v3.5.1
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c // indirect
	golang.org/x/sys v0.1.0 // indirect
	golang.org/x/term v0.0.0-20210927222741-03fcf44c2211 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
